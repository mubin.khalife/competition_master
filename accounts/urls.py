from django.urls import path
from .views import register
from django.contrib.auth import views as auth_views

app_name = "accounts"

urlpatterns = [
    # path('login/', login, name="login"),
    path('login/', auth_views.LoginView.as_view(
        template_name="accounts/login.html", redirect_authenticated_user=True),
        name="login"),
    path('register/', register, name="register"),
    path('logout/', auth_views.LogoutView.as_view(
        template_name="accounts/logout.html"), name='logout'),
    # path('', dummy, name="dummy"),
]
