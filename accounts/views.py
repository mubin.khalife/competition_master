from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm


def register(request):
    if request.user.is_anonymous:
        if request.method == "POST":
            form = UserRegisterForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get("username")
                messages.success(request, f'Account created for {username}! \
                                You can now login')
                return redirect("accounts:login")
                # return render(request, "accounts/login.html")
            # else:
            #     messages.error(request, form.error_messages)
        else:
            form = UserRegisterForm()

        return render(request, "accounts/register.html", {'form': form})

    else:
        # return render(request, "accounts/login.html")
        return redirect("home:index")

# def login(request):
    # return HttpResponse("Login")
    # return render(request, "accounts/login.html")


# def dummy(request):
#     return HttpResponse("Howdy!")
