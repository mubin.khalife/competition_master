from django.contrib import admin
from .models import Game, GameScore


@admin.register(Game)
class GameModelAdmin(admin.ModelAdmin):
    readonly_fields = ('slug',)


admin.site.unregister(Game)
admin.site.register(Game, GameModelAdmin)
admin.site.register(GameScore)
