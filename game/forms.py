from django import forms

from .models import Game, GameScore


class GameForm(forms.ModelForm):
    description = forms.CharField(required=True, widget=forms.widgets.Textarea(
        attrs={"placeholder": "Game Description", "class": "form-control"}))

    class Meta:
        model = Game
        fields = "__all__"


class UpdateGameScore(forms.ModelForm):
    class Meta:
        model = GameScore
        exclude = ("game", "user", "score", "last_played")
