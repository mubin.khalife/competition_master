from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify


class Game(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(allow_unicode=True, unique=True, blank=True)
    description = models.CharField(max_length=1000)

    def __str__(self):
        """
        Name of the game
        """
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    # def get_absolute_url(self):
    #     return reverse('games:game_detail', kwargs={'slug': self.slug})


class GameScore(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField()
    # highest_score = models.IntegerField()
    # last_played = models.DateTimeField(default=timezone.now)
    last_played = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-score',)

    def __str__(self):
        """
        Id of the scoreboard
        """
        return str(self.pk)
