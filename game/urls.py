from django.urls import path
from .views import Games, GameDetailView

app_name = "games"

urlpatterns = [
    path('', Games.as_view(), name="games_list"),
    path('<str:slug>/', GameDetailView.as_view(), name="game_detail"),
]
