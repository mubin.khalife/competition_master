from django.shortcuts import redirect
from django.views import generic
from .models import Game, GameScore
from .forms import UpdateGameScore
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import JsonResponse
from django.utils import timezone
# from django.core.exceptions import *
# from django.core.exceptions import MultipleObjectsReturned


class Games(LoginRequiredMixin, UserPassesTestMixin, generic.ListView):
    model = Game
    ordering = ["-name"]

    def test_func(self):
        if self.request.user.is_superuser:
            return False
        else:
            return True

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.get_test_func()()
        if not user_test_result:
            return redirect('admin:index')
            # return self.handle_no_permission()
        return super(UserPassesTestMixin, self).dispatch(request,
                                                         *args, **kwargs)


class GameDetailView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    model = Game
    # form_class = UpdateGameScore
    # template_name = "game/game_detail.html"
    highest_score = 0
    best_score = 0
    game_obj = None

    def test_func(self):
        if self.request.user.is_superuser:
            return False
        elif self.request.user.is_anonymous:
            return False
        else:
            return True

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.get_test_func()()
        if not user_test_result:
            if self.request.user.is_superuser:
                return redirect('admin:index')
            else:
                return redirect('home:index')
            # return self.handle_no_permission()
        return super(UserPassesTestMixin, self).dispatch(request,
                                                         *args, **kwargs)

    def get_cust_object(self, req, game_obj):
        # print(self.object.id)
        best_score_query = GameScore.objects.filter(
            user=req.user, game=game_obj).\
            order_by('-score')[:1]
        highest_score_query = GameScore.objects.filter(game=game_obj).\
            order_by('-score')[:1]
        # highest_score_query = GameScore.objects.filter(game=self.game_obj)\
        #     .order_by("-score").first()

        best_score = 0
        highest_score = 0
        highest_scorer = ""

        if len(best_score_query) == 0:
            best_score = 0
        else:
            best_score = best_score_query[0].score

        if len(highest_score_query) == 0:
            retObj = {"best_score": 0,
                      "highest_score": 0,
                      "highest_scorer": req.user.username,
                      }
            return retObj
        else:
            if len(highest_score_query) > 0:
                highest_score = highest_score_query[0].score
                if highest_score_query[0].user == req.user:
                    highest_scorer = "You"
                else:
                    highest_scorer = highest_score_query[0].user.username

                retObj = {"best_score": str(best_score),
                          "highest_score": str(highest_score),
                          "highest_scorer": highest_scorer,
                          }

                return retObj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.game_obj = self.object
        # print(context["object"].slug)
        # print(context["game"].slug)

        score_object = self.get_cust_object(
            self.request, self.game_obj)
        # str(best_score_query.score)
        context["best_score"] = score_object["best_score"]

        # highest_score_query = GameScore.objects.all().first()

        # str(highest_score_query.score)
        context["highest_score"] = score_object["highest_score"]
        context["highest_scorer"] = score_object["highest_scorer"]

        return context

    def post(self, request, **kwargs):
        form = UpdateGameScore(data=request.POST)
        # form = self.form_class(data=request.POST)

        if form.is_valid():
            game_frm_data = request.POST
            game_frm = form.save(commit=False)
            game_frm.game = Game.objects.get(pk=game_frm_data["gameObj"])
            game_frm.user = request.user
            game_frm.score = game_frm_data["last_score"]
            game_frm.last_played = timezone.now
            game_frm.save()

            # self.object = self.get_object()
            score_object = self.get_cust_object(
                request, game_frm_data["gameObj"])
            if score_object is not None:
                highest_scorer = score_object["highest_scorer"]
                highest_score = int(score_object["highest_score"])
                best_score = int(score_object["best_score"])
                last_score = int(game_frm_data["last_score"])

                if last_score > highest_score:
                    game_new_data = {"best_score": last_score,
                                     "highest_score": last_score,
                                     "highest_scorer": highest_scorer,
                                     }
                elif last_score > best_score:
                    game_new_data = {"best_score": last_score,
                                     "highest_score": highest_score,
                                     "highest_scorer": highest_scorer
                                     }
                else:
                    game_new_data = {"best_score": best_score,
                                     "highest_score": highest_score,
                                     "highest_scorer": highest_scorer
                                     }

                return JsonResponse(game_new_data)
        else:
            print(form.errors)
            return JsonResponse({'status': 'false', 'message':
                                 "form is invalid"}, status=500)
        # return super(TemplateView, self).render_to_response(context)
