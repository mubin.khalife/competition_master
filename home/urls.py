from django.urls import path
from .views import landingpage, dashboard

app_name = "home"

urlpatterns = [
    path('', landingpage, name="index"),
    path('dashboard/', dashboard, name="dashboard"),
    path('test/', landingpage, name="test"),
]
