from django.shortcuts import render, redirect
# from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from game.models import GameScore
from quiz.models import QuizScore
from django.db.models.aggregates import Max


@login_required
def landingpage(request):
    # print(request.META.get('HTTP_REFERER'))

    # Other values that you can find in the values_list include:
    # id, uid (for ex. facebook user id), user, extra_data
    # (which contains the access_token)
    # print(request.user.social_auth.values_list('provider'))

    # print(request.user.social_auth.values_list('provider')[0][0])
    # if not request.user.is_anonymous and \
    # if request.user.social_auth.filter(user=request.user).count() > 0:
    #     provider = request.user.social_auth.values_list('provider')[0][0]
    #     user_exists = User.objects.filter(username=request.user.username)

    #     if 'google' in provider:
    #         provider = "google"

    #     if user_exists.count() > 0:
    #         social_user = User.objects.filter(username=request.user.username)

    #         social_user.update(username=request.user.username+"_"+provider)
            # social_user.update(username=request.user.social_auth.values_list
            #                    ('extra_data')[0][0]["login"]+"_"+provider)

        # print("<Social>" + request.user.social_auth.values_list
        #       ('extra_data')[0][0]["login"])

    # else:
    #     print("<Local> "+request.user.username)
    if request.user.is_superuser:
        return redirect('admin:index')
    else:
        return render(request, "index.html")


@login_required
def dashboard(request):
    # return HttpResponse("Hello")
    if request.user.is_superuser:
        return redirect('admin:index')
    else:
        # get top scorers
        game_ids = []
        game_model = GameScore.objects.all().order_by("-score")
        game_top_scorers = {}
        for item in game_model:
            if len(game_top_scorers) == 3:
                break

            if item.game_id not in game_ids:
                game_ids.append(item.game_id)
                game_top_scorers[item.game] = {"user": item.user.username,
                                               "score": item.score}
        topic_ids = []
        quiz_model = QuizScore.objects.all().order_by("-score")
        quiz_top_scorers = {}
        for item in quiz_model:
            if len(quiz_top_scorers) == 3:
                break
            if item.topic_id not in topic_ids:
                topic_ids.append(item.topic_id)
                quiz_top_scorers[item.topic] = {"user": item.user.username,
                                                "score": item.score}

        # print(game_top_scorers)
        # print(quiz_top_scorers)
        top_score_context = {
            "games": game_top_scorers, "quiz": quiz_top_scorers}
        return render(request, "dashboard.html", top_score_context)


# def login(request):
#     return render(request, "login.html")
