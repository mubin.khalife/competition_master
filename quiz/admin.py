from django.contrib import admin
from .models import Topic, Quiz_Question, Quiz_Option
from .forms import QuizOptionForm


@admin.register(Quiz_Option)
class QuizAdmin(admin.ModelAdmin):
    form = QuizOptionForm


# admin.site.unregister(Topic)
admin.site.register(Topic)
admin.site.register(Quiz_Question)
admin.site.unregister(Quiz_Option)
admin.site.register(Quiz_Option, QuizAdmin)
