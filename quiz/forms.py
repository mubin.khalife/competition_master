import itertools
from django import forms
from django.utils.text import slugify
from .models import Topic, Quiz_Option, QuizScore
from django.core.exceptions import ValidationError


class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = (
            'name',
        )

    # To avoid unique constraint error in slug
    # e.g .C Programming and C++ programming
    # generates this error while adding them
    def save(self):
        instance = super(TopicForm, self).save(commit=False)

        instance.slug = orig = slugify(instance.title)

        for x in itertools.count(1):
            if not Topic.objects.filter(slug=instance.slug).exists():
                break
            instance.slug = '%s-%d' % (orig, x)

        instance.save()

        return instance


class QuizOptionForm(forms.ModelForm):
    class Meta:
        model = Quiz_Option
        fields = '__all__'

    def clean(self):
        data = self.cleaned_data
        question_text = self.cleaned_data["question"]

        if Quiz_Option.objects.filter(question=question_text).\
                exclude(pk=self.instance.pk).count() >= 4:
            raise ValidationError("Maximum four options allowed per question!")

        if Quiz_Option.objects.filter(question=question_text,
                                      correct_option=True).\
            exclude(pk=self.instance.pk)\
                .count() > 1:
            raise ValidationError("Only one correct option allowed.\
                                  This question already has correct \
                                      option set")

        # print("---------------")
        # print(str(data["correct_option"]))
        # print(Quiz_Option.objects.filter(question=question_text,
        #                                  correct_option=False).count())

        if Quiz_Option.objects.filter(question=question_text,
                                      correct_option=False).\
                count() >= 3 and \
                data["correct_option"] is False:
            raise ValidationError("This question has no correct \
                                      option set. Please set atleast one \
                                          option as correct")
        return data


# class UpdateQuizScore(forms.ModelForm):
#     class Meta:
#         model = QuizScore
#         exclude = '__all__'
