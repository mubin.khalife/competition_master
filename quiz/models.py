from django.db import models
# from django.shortcuts import reverse
from django.contrib.auth.models import User


class Topic(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(allow_unicode=False, unique=True,
                            blank=True)  # , default=uuid.uuid1)

    # def get_absolute_url(self):
    #     return reverse('quiz:quiz_detail', kwargs={'slug': self.slug})

    def __str__(self):
        """
        Name of the Topic
        """
        return self.name

    # def save(self, *args, **kwargs):
    #     self.slug = slugify(self.name)
    #     super().save(*args, **kwargs)


class Quiz_Question(models.Model):
    topic = models.ForeignKey(
        Topic, on_delete=models.CASCADE, related_name='topic')
    question_text = models.CharField(max_length=1000, blank=False, default='')
    # question_text = models.ForeignKey('Quiz_Option', on_delete=models.CASCADE)

    def __str__(self):
        """
        Text of the question
        """
        return self.question_text

    def get_options(self):
        return Quiz_Option.objects.filter(
            question__pk=self.pk
        )

    question_related_options = property(get_options)


class Quiz_Option(models.Model):
    question = models.ForeignKey(Quiz_Question,
                                 on_delete=models.CASCADE,
                                 related_name='questions', blank=False,
                                 default='')
    question_option = models.CharField(max_length=300)
    correct_option = models.BooleanField(default=False)

    def __str__(self):
        """
        Text of the option
        """
        return self.question_option


class QuizLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    topic = models.ForeignKey(Topic, on_delete=models.DO_NOTHING)
    # question = models.ForeignKey(Quiz_Question, on_delete=models.DO_NOTHING)
    question_id = models.IntegerField(default=0)
    # option = models.ForeignKey(Quiz_Option, on_delete=models.DO_NOTHING)
    option_id = models.IntegerField(default=0)
    # submitted = models.BooleanField(default=False)

    def __str__(self):
        """
        Username of the competitor
        """
        return self.user.username


class QuizScore(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    topic = models.ForeignKey(Topic, on_delete=models.DO_NOTHING, default='')
    score = models.IntegerField(default=0)

    def __str__(self):
        """
        Username of the competitor
        """
        return self.user.username
