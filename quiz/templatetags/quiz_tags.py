from django import template

register = template.Library()


question_list = []
option_list = []


@register.filter(name='exist_check_filter')
def checkQuestionInList(value):
    return value in question_list


@register.filter(name='set_filter')
def setList(value):
    if value not in question_list:
        question_list.append(value)


@register.filter(name='counter_filter')
def getCurrIndexOfList(value):
    if value in question_list:
        return (question_list.index(value)+1)
    else:
        return 1


@register.filter(name='check_new_question')
def bNewQuestion(dummy):
    if len(option_list) % 4 == 0:
        return True
    else:
        return False


@register.filter(name='set_option_list_filter')
def set_option_list(val):
    option_list.append(val)


@register.filter(name='all_option_list_filter')
def all_option_list(val):
    if len(option_list) % 4 == 0:
        return True
    else:
        return False


@register.filter(name='get_option_count')
def get_option_count(val):
    return len(option_list)


@register.filter(name='clear_option_count')
def clear_option_count(val):
    return option_list.clear()


# register.filter('q_filter_check', checkQuestionInList)
# register.filter('g_list', getList)
# register.filter('s_list', setList)
