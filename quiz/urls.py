from django.urls import path
from .views import Topic, QuizDetailView

app_name = "quiz"

urlpatterns = [
    path('', Topic.as_view(), name="topic_list"),
    path('<str:slug>/', QuizDetailView.as_view(), name="quiz_detail"),
]
