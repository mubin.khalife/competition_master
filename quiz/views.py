from django.shortcuts import redirect
from django.views import generic
from .models import (Topic as TopicModel, Quiz_Option,
                     Quiz_Question, QuizScore, QuizLog)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import JsonResponse
from django.db.models import Count, Max
import json
from django.db import transaction


class Topic(LoginRequiredMixin, UserPassesTestMixin, generic.ListView):
    model = TopicModel
    ordering = ["name"]

    def test_func(self):
        if self.request.user.is_superuser:
            return False
        elif self.request.user.is_anonymous:
            return False
        else:
            return True

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.get_test_func()()
        if not user_test_result:
            if self.request.user.is_superuser:
                return redirect('admin:index')
            else:
                return redirect('home:index')
            # return self.handle_no_permission()
        return super(UserPassesTestMixin, self).dispatch(request,
                                                         *args, **kwargs)


class QuizDetailView(LoginRequiredMixin, UserPassesTestMixin,
                     generic.DetailView):
    model = Quiz_Question
    template_name = "quiz/quiz_detail.html"

    # slug_url_kwarg = "not_slug"
    slug_field = "topic__slug"

    def test_func(self):
        if self.request.user.is_superuser:
            return False
        elif self.request.user.is_anonymous:
            return False
        else:
            return True

    def dispatch(self, request, *args, **kwargs):
        user_test_result = self.get_test_func()()
        if not user_test_result:
            if self.request.user.is_superuser:
                return redirect('admin:index')
            else:
                return redirect('home:index')
            # return self.handle_no_permission()
        return super(UserPassesTestMixin, self).dispatch(request,
                                                         *args, **kwargs)

    def get_object(self):
        topic_id = TopicModel.objects.filter(
            slug=self.kwargs['slug']).values_list('id', flat=True)

        question_ids = Quiz_Question.objects.filter(
            topic__in=topic_id).values_list('id', flat=True)

        invalid_options_count = Quiz_Option.objects.values("question__id").\
            annotate(option_count=Count('question__id')).\
            filter(option_count__lt=4)

        ignore_questions = []

        for item in invalid_options_count:
            ignore_questions.append(item["question__id"])

        return Quiz_Question.objects.values("id", "question_text",
                                            "topic__name", "topic__slug").\
            filter(id__in=list(question_ids)).\
            exclude(id__in=ignore_questions)

    def get_context_data(self, **kwargs):
        # print(self.kwargs['slug'])

        # Selected topic questions
        context = super().get_context_data(**kwargs)

        topic_name = self.kwargs['slug']
        topic_obj = TopicModel.objects.get(slug=topic_name)

        optionQuery = self.get_custom_data(topic_name)

        question_option_dict = {}
        correct_question_option = {}
        valid_questions = []

        for option in optionQuery:
            if option["question_id"] in question_option_dict:
                question_option_dict[option["question_id"]].append(
                    option)
            else:
                valid_questions.append(option["question_id"])
                question_option_dict[option["question_id"]] = []
                question_option_dict[option["question_id"]].append(option)

            if option["correct_option"]:
                correct_question_option[option["question_id"]] = option["id"]

        context["question_option_dict"] = question_option_dict
        # Logic to get scores
        quizScoreQuery = QuizScore.objects.filter(topic=topic_obj,)

        if len(quizScoreQuery) == 0:
            context["best_score"] = 0
            context["highest_score"] = 0
            context["highest_scorer"] = ""
        else:
            quizUserScoreQuery = QuizScore.objects.\
                filter(topic=topic_obj, user=self.request.user)
            user_score_query = quizUserScoreQuery.aggregate(Max('score'))
            if user_score_query['score__max'] is None:
                user_score = 0
            else:
                user_score = user_score_query['score__max']

            highest_score_query = QuizScore.objects.all().\
                order_by('-score')[:1]
            # highest_score_query = QuizScore.objects.aggregate(Max('score'))

            if highest_score_query is None:
                highest_score = 0
            else:
                highest_score = highest_score_query[0].score
                # highest_score = highest_score_query['score__max']

            context["best_score"] = user_score
            context["highest_score"] = highest_score

            if highest_score_query[0].user == self.request.user:
                context["highest_scorer"] = "You"
            else:
                context["highest_scorer"] = highest_score_query[0].\
                    user.username

        return context

    def get_custom_data(self, topic_name):
        topic_id = TopicModel.objects.filter(
            slug=topic_name).values_list('id', flat=True)
        question_ids = Quiz_Question.objects.filter(
            topic__in=topic_id).values_list('id', flat=True)

        invalid_options_count = Quiz_Option.objects.values("question__id").\
            annotate(option_count=Count('question__id')).\
            filter(option_count__lt=4)

        ignore_questions = []

        for item in invalid_options_count:
            ignore_questions.append(item["question__id"])

        optionQuery = Quiz_Option.objects.\
            filter(question_id__in=list(question_ids)).\
            exclude(question_id__in=ignore_questions).\
            values("id", "question_id", "question_option", "correct_option")

        return optionQuery

    def post(self, request, **kwargs):
        # print(request.POST)

        post_data = json.loads(request.POST.get('values'))
        topic = request.POST.get('topic')

        topic_obj = TopicModel.objects.get(slug=topic)

        optionQuery = self.get_custom_data(topic)

        correct_question_option = {}

        for option in optionQuery:
            if option["correct_option"]:
                correct_question_option[option["question_id"]] = option["id"]

        correct_count = 0
        for key, value in post_data.items():
            if int(key) in correct_question_option and \
                    correct_question_option[int(key)] == value:
                correct_count += 1

        score = int((correct_count/(len(post_data)))*100)

        # Save score
        quiz_score_entry = QuizScore(
            user=self.request.user, topic=topic_obj, score=score)
        quiz_score_entry.save()

        # Save log
        with transaction.atomic():
            for key, value in post_data.items():
                entry = QuizLog(user=self.request.user, topic=topic_obj,
                                question_id=key, option_id=value)
                entry.save()

        quizScoreQuery = QuizScore.objects.filter(topic=topic_obj,)

        if len(quizScoreQuery) == 0:
            best_score = score
            highest_score = score
        else:
            quizUserScoreQuery = QuizScore.objects.\
                filter(topic=topic_obj, user=self.request.user)
            user_score_query = quizUserScoreQuery.aggregate(Max('score'))
            if user_score_query['score__max'] is None:
                best_score = 0
            else:
                best_score = user_score_query['score__max']

            # highest_score_query = QuizScore.objects.aggregate(Max('score'))
            # if highest_score_query['score__max'] is None:
            #     highest_score = 0
            # else:
            #     highest_score = highest_score_query['score__max']
            highest_scorer = ""
            highest_score_query = QuizScore.objects.all().order_by('score')[:1]
            # highest_score_query = QuizScore.objects.aggregate(Max('score'))
            if highest_score_query is None:
                highest_score = 0
            else:
                highest_score = highest_score_query[0].score
                if highest_score_query[0].user == self.request.user:
                    highest_scorer = "You"
                else:
                    highest_scorer = highest_score_query[0].user.username

                # highest_score = highest_score_query['score__max']
        # return JsonResponse({"success": "success"})
        return JsonResponse({"success": "success",
                             "best_score": int(best_score),
                             "highest_score": int(highest_score),
                             "highest_scorer": highest_scorer,
                             })
